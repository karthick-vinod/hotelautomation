package com.sahaj.config;

import com.sahaj.hotel.Appliance.ApplianceType;
import com.sahaj.hotel.Corridor.CorridorType;
import com.sahaj.hotel.Hotel.Mode;

import java.util.HashMap;
import java.util.Map;

public class ConfigurationBuilder {

    Map<Mode, Map<CorridorType, Map<ApplianceType, ConfigEntity>>> config = new HashMap<>();

    public ModeBuilder mode(Mode mode) {
        config.put(mode, new HashMap<>());
        return new ModeBuilder(mode, this);
    }


    public Map<Mode, Map<CorridorType, Map<ApplianceType, ConfigEntity>>> build() {
        return config;
    }

    public static class ModeBuilder {
        Map<CorridorType, Map<ApplianceType, ConfigEntity>> modeConfig = new HashMap<>();
        Mode mode;
        ConfigurationBuilder configurationBuilder;

        public ModeBuilder(Mode mode, ConfigurationBuilder parent) {
            this.configurationBuilder = parent;
            this.mode = mode;
        }

        public ModeBuilder buildFromConfigEntity(ConfigEntity configEntity) {
            if (!modeConfig.containsKey(configEntity.getCorridorType())) {
                modeConfig.put(configEntity.getCorridorType(), new HashMap<>());

            }
            modeConfig.get(configEntity.getCorridorType()).put(configEntity.getApplianceType(), configEntity);
            return this;
        }

        public ConfigurationBuilder buildMode() {
            this.configurationBuilder.config.put(this.mode, modeConfig);
            return this.configurationBuilder;
        }
    }
}
