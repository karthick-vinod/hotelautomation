package com.sahaj.config;

import com.sahaj.hotel.Appliance;
import com.sahaj.hotel.Corridor;
import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class ConfigEntity {

    /**
     * Type of the corridor.
     * @see com.sahaj.hotel.Corridor.CorridorType
     */
    private Corridor.CorridorType corridorType;

    /**
     * Type of the appliance.
     * @see com.sahaj.hotel.Appliance.ApplianceType
     */
    private Appliance.ApplianceType applianceType;

    /**
     * Initial state of the appliance.
     * @see com.sahaj.hotel.Appliance.State
     */
    private Appliance.State state;

    /**
     * Number of appliances for the given {@code corridorType}
     */
    private Integer applianceCount;

    /**
     * Units of power that the appliance consumes.
     */
    private Float appliancePower;

    /**
     * Is this appliance mandatory.
     * If true, it will not be switched of at any cost.
     */
    private Boolean mandatory;
}
