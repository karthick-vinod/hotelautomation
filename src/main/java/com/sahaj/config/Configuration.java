package com.sahaj.config;

import com.sahaj.hotel.Appliance;
import com.sahaj.hotel.Corridor;
import com.sahaj.hotel.Hotel;

import java.util.HashMap;
import java.util.Map;

import static com.sahaj.hotel.Appliance.ApplianceType.AC;
import static com.sahaj.hotel.Appliance.ApplianceType.LIGHT;
import static com.sahaj.hotel.Appliance.State.OFF;
import static com.sahaj.hotel.Appliance.State.ON;
import static com.sahaj.hotel.Corridor.CorridorType.MAIN;
import static com.sahaj.hotel.Corridor.CorridorType.SUB;
import static com.sahaj.hotel.Hotel.Mode.DAY;
import static com.sahaj.hotel.Hotel.Mode.NIGHT;

public class Configuration {

    private static Map<Hotel.Mode, Map<Corridor.CorridorType, Map<Appliance.ApplianceType, ConfigEntity>>> config = new HashMap<>();

    private static Hotel.Mode mode = NIGHT;

    private static Map<Corridor.CorridorType, Map<Appliance.ApplianceType, ConfigEntity>> getConfig() {
        return config.get(Configuration.mode);
    }

    // TODO: Add configuration for DAY mode
    static {

        ConfigurationBuilder c = new ConfigurationBuilder().mode(Hotel.Mode.NIGHT)
                .buildFromConfigEntity(new ConfigEntity(MAIN, AC, ON, 1, 10F, true))
                .buildFromConfigEntity(new ConfigEntity(MAIN, LIGHT, ON, 1, 5F, true))
                .buildFromConfigEntity(new ConfigEntity(SUB, AC, ON, 1, 10F, true))
                .buildFromConfigEntity(new ConfigEntity(SUB, LIGHT, OFF, 1, 5F, false))
                .buildMode();

        Configuration.setConfig(c);
    }

    public static void setConfig(ConfigurationBuilder builder) {
        Configuration.config = builder.build();
    }

    public static Map<Appliance.ApplianceType, ConfigEntity> get(Corridor.CorridorType corridorType) {
        return getConfig().get(corridorType);
    }

    public static ConfigEntity get(Corridor.CorridorType corridorType, Appliance.ApplianceType applianceType) {
        Map<Appliance.ApplianceType, ConfigEntity> corridorConfig = getConfig().get(corridorType);
        assert corridorConfig != null : "Cannot find config for corridor type" + corridorType;
        return corridorConfig.get(applianceType);
    }

}
