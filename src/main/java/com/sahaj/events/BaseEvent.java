package com.sahaj.events;

import com.sahaj.events.enums.IEventType;
import com.sahaj.hotel.Corridor;
import com.sahaj.hotel.Floor;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class BaseEvent<T extends IEventType> {
    private T eventType;
    private Floor floor;
    private Corridor corridor;
}
