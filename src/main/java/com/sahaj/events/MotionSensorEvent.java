package com.sahaj.events;

import com.sahaj.events.enums.MotionSensorEventType;
import com.sahaj.hotel.Corridor;
import com.sahaj.hotel.Floor;

public class MotionSensorEvent extends BaseEvent<MotionSensorEventType> {
    public MotionSensorEvent(MotionSensorEventType eventType, Floor floor, Corridor corridor) {
        super(eventType, floor, corridor);
    }
}
