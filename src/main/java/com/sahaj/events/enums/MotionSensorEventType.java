package com.sahaj.events.enums;

public enum MotionSensorEventType implements IEventType {
    MOTION_DETECTED,
    MOTION_HALTED
}
