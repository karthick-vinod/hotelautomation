package com.sahaj.controllers;

import com.sahaj.config.ConfigEntity;
import com.sahaj.config.Configuration;
import com.sahaj.events.BaseEvent;
import com.sahaj.events.MotionSensorEvent;
import com.sahaj.hotel.Appliance;
import com.sahaj.hotel.Corridor;
import com.sahaj.hotel.Floor;
import com.sahaj.hotel.Hotel;
import lombok.Data;

import java.util.Comparator;
import java.util.Map;
import java.util.Optional;

import static com.sahaj.hotel.Appliance.ApplianceType.AC;
import static com.sahaj.hotel.Appliance.ApplianceType.LIGHT;

import static com.sahaj.hotel.Appliance.State.ON;
import static com.sahaj.hotel.Appliance.State.OFF;

import static com.sahaj.hotel.Corridor.CorridorType.SUB;

@Data
public class HotelAutomationBaseController {

    private static final Map<Appliance.ApplianceType, ConfigEntity> applianceMap = Configuration.get(SUB);
    private final Hotel hotel;

    public HotelAutomationBaseController(Hotel hotel) {
        this.hotel = hotel;
        System.out.println(this.hotel);
    }

    public void processEvent(BaseEvent event) {
        try {
            if (event instanceof MotionSensorEvent) {
                processMotionSensorEvent((MotionSensorEvent) event);
            } else {
                System.out.println(event);
                System.out.println("Unrecognized event obtained. Skipping...");
            }
        } catch (Exception e) {
            System.out.println("ERROR: " + e.getMessage());
        }

        System.out.println(this.hotel);
    }

    private void processMotionSensorEvent(MotionSensorEvent event) throws Exception {
        switch (event.getEventType()) {
            case MOTION_DETECTED:
                processMotionDetectedEvent(event);
                break;
            case MOTION_HALTED:
                this.processMotionHaltedEvent(event);
                break;
            default:
                System.out.println("Unrecognized motionSensorEvent obtained. Skipping...");
        }
    }

    private void processMotionDetectedEvent(MotionSensorEvent event) throws Exception {
        Floor floor = event.getFloor();
        Corridor corridor = event.getCorridor();
        if(corridor.getType() == SUB && !corridor.getMotionInProgress()) {
            corridor.setMotionInProgress(true);
            Optional<Appliance> lightToTurnOn = corridor.getAppliances().stream().filter(a -> a.getState() == OFF && a.getType() == LIGHT).findFirst();

            if(lightToTurnOn.isPresent()) {
                lightToTurnOn.get().turnOn();

                if(floor.isPowerConsumptionAboveThreshold()) {

                    Float powerToReduce = floor.computeCurrentPowerConsumption() - floor.getMaxAllowedPowerConsumption();
                    int countOfACsToTurnOff = (int) Math.ceil(powerToReduce / applianceMap.get(Appliance.ApplianceType.AC).getAppliancePower());

                    // Select an AC on a sub-corridor without motion and turn it off
                    floor.getSubCorridors().stream()
                            .sorted(Comparator.comparing(Corridor::getMotionInProgress, Boolean::compare))
                            .flatMap(corridor1 -> corridor1.getAppliances().stream())
                            .filter(a -> a.getState() == ON && a.getType() == AC)
                            .limit(countOfACsToTurnOff)
                            .forEach(Appliance::turnOff);

                    if(floor.isPowerConsumptionAboveThreshold()) {
                        throw new Exception("No unnecessary appliance to switch off and save energy ");
                    }

                }
            } else {
                // TODO: Handle cases where all lights are turned ON before event's receipt
                throw new Exception("Cannot find a LIGHT to turn on");
            }
        }

    }

    private void processMotionHaltedEvent(MotionSensorEvent event) {
        Floor floor = event.getFloor();
        Corridor corridor = event.getCorridor();

        if(corridor.getType() == SUB && corridor.getMotionInProgress()) {
            // Switch off the lights
            corridor.getAppliances().stream().filter(a -> a.getState() == ON && a.getType() == LIGHT).forEach(Appliance::turnOff);
            corridor.setMotionInProgress(false);
        }

        // Turn On ACs as necessary
        Float remainingPower = floor.getMaxAllowedPowerConsumption() - floor.computeCurrentPowerConsumption();
        int countOfACsToTurnOn = (int) Math.floor(remainingPower / applianceMap.get(Appliance.ApplianceType.AC).getAppliancePower());

        floor.getSubCorridors().stream()
                .flatMap(c -> c.getAppliances().stream())
                .filter(appliance -> appliance.getType() == AC && appliance.getState() == OFF)
                .limit(countOfACsToTurnOn).forEach(Appliance::turnOn);
    }

}
