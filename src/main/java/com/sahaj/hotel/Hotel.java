package com.sahaj.hotel;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

@Data
public class Hotel {

    /**
     * Mode of operation of hotel
     */
    public enum Mode {
        DAY,
        NIGHT
    }

    private Integer floorsCount;
    private Integer mainCorridorsPerFloor;
    private Integer subCorridorsPerFloor;

    private List<Floor> floors = new ArrayList<>();

    public Hotel(Integer floors, Integer mainCorridorsPerFloor, Integer subCorridorsPerFloor) {
        this.floorsCount = floors;
        this.mainCorridorsPerFloor = mainCorridorsPerFloor;
        this.subCorridorsPerFloor = subCorridorsPerFloor;
        this.initializeFloors();
    }

    private void initializeFloors() {
        IntStream.range(1, this.floorsCount + 1).forEachOrdered(i -> this.floors.add(new Floor(i, this.mainCorridorsPerFloor, this.subCorridorsPerFloor)));
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        floors.forEach(f -> builder.append(f.toString()));
        return builder.append(System.lineSeparator()).append("=============================").append(System.lineSeparator()).toString();
    }
}
