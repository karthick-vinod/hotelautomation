package com.sahaj.hotel;

import com.sahaj.config.ConfigEntity;
import com.sahaj.config.Configuration;
import com.sahaj.config.ConfigurationBuilder;
import com.sahaj.controllers.HotelAutomationBaseController;
import com.sahaj.events.MotionSensorEvent;
import com.sahaj.events.enums.MotionSensorEventType;

import static com.sahaj.hotel.Appliance.ApplianceType.AC;
import static com.sahaj.hotel.Appliance.ApplianceType.LIGHT;
import static com.sahaj.hotel.Appliance.State.OFF;
import static com.sahaj.hotel.Appliance.State.ON;
import static com.sahaj.hotel.Corridor.CorridorType.MAIN;

public class Main {
    /**
     * Use this main function to try out custom scenarios.
     * Alternatively, {@code HotelAutomationBaseControllerTest} unit tests can also be used
     */
    public static void main(String[] args) {

        // Custom configuration. Can be ignored to use default configuration.
        ConfigurationBuilder c = new ConfigurationBuilder().mode(Hotel.Mode.NIGHT)
                .buildFromConfigEntity(new ConfigEntity(MAIN, AC, ON, 1, 10F, true))
                .buildFromConfigEntity(new ConfigEntity(MAIN, LIGHT, ON, 1, 5F, true))
                .buildFromConfigEntity(new ConfigEntity(Corridor.CorridorType.SUB, AC, ON, 1, 10F, true))
                .buildFromConfigEntity(new ConfigEntity(Corridor.CorridorType.SUB, LIGHT, OFF, 1, 5F, false))
                .buildMode();

        Configuration.setConfig(c);

        scenario1();

        scenario2();

    }

    static void scenario1() {
        System.out.println("Scenario-1 : Normal case");
        Hotel hotel = new Hotel(1, 1, 4);

        HotelAutomationBaseController hotelController = new HotelAutomationBaseController(hotel);

        hotelController.processEvent(new MotionSensorEvent(MotionSensorEventType.MOTION_DETECTED, hotel.getFloors().get(0), hotel.getFloors().get(0).getSubCorridors().get(0)));
        hotelController.processEvent(new MotionSensorEvent(MotionSensorEventType.MOTION_HALTED, hotel.getFloors().get(0), hotel.getFloors().get(0).getSubCorridors().get(0)));
    }

    static void scenario2() {
        System.out.println("Scenario-2 : Controller should gracefully handle exceeded power consumption");
        Hotel hotel = new Hotel(1, 1, 4);

        HotelAutomationBaseController hotelController = new HotelAutomationBaseController(hotel);

        hotelController.processEvent(new MotionSensorEvent(MotionSensorEventType.MOTION_DETECTED, hotel.getFloors().get(0), hotel.getFloors().get(0).getSubCorridors().get(0)));
        hotelController.processEvent(new MotionSensorEvent(MotionSensorEventType.MOTION_DETECTED, hotel.getFloors().get(0), hotel.getFloors().get(0).getSubCorridors().get(1)));
        hotelController.processEvent(new MotionSensorEvent(MotionSensorEventType.MOTION_DETECTED, hotel.getFloors().get(0), hotel.getFloors().get(0).getSubCorridors().get(2)));
        hotelController.processEvent(new MotionSensorEvent(MotionSensorEventType.MOTION_DETECTED, hotel.getFloors().get(0), hotel.getFloors().get(0).getSubCorridors().get(3)));

        hotelController.processEvent(new MotionSensorEvent(MotionSensorEventType.MOTION_HALTED, hotel.getFloors().get(0), hotel.getFloors().get(0).getSubCorridors().get(0)));
        hotelController.processEvent(new MotionSensorEvent(MotionSensorEventType.MOTION_HALTED, hotel.getFloors().get(0), hotel.getFloors().get(0).getSubCorridors().get(1)));
        hotelController.processEvent(new MotionSensorEvent(MotionSensorEventType.MOTION_HALTED, hotel.getFloors().get(0), hotel.getFloors().get(0).getSubCorridors().get(2)));
        hotelController.processEvent(new MotionSensorEvent(MotionSensorEventType.MOTION_HALTED, hotel.getFloors().get(0), hotel.getFloors().get(0).getSubCorridors().get(3)));
    }
}
