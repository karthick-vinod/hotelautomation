package com.sahaj.hotel;

import com.sahaj.config.ConfigEntity;
import com.sahaj.config.Configuration;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static com.sahaj.hotel.Corridor.CorridorType.MAIN;
import static com.sahaj.hotel.Corridor.CorridorType.SUB;

@Data
public class Floor {
    private Integer floorId;
    private List<Corridor> mainCorridors = new ArrayList<>();
    private List<Corridor> subCorridors = new ArrayList<>();

    private final Float maxAllowedPowerConsumption;

    public Floor(Integer floorId, Integer mainCorridorCount, Integer subCorridorCount) {
        this.floorId = floorId;
        IntStream.range(1, mainCorridorCount + 1).forEachOrdered(i -> this.mainCorridors.add(new Corridor(i, MAIN)));
        IntStream.range(1, subCorridorCount + 1).forEachOrdered(i -> this.subCorridors.add(new Corridor(i, SUB)));

        maxAllowedPowerConsumption = computeMaxAllowedPowerConsumption();
    }

    private Float computeMaxAllowedPowerConsumption() {
        float result = 0F;
        for (Corridor.CorridorType corridorType : Corridor.CorridorType.values()) {
            Map<Appliance.ApplianceType, ConfigEntity> config = Configuration.get(corridorType);
            int corridorCount = corridorType == MAIN ? mainCorridors.size() : subCorridors.size();
            for(ConfigEntity configEntity: config.values()) {
                if (configEntity.getMandatory())
                    result = result + (configEntity.getAppliancePower() * configEntity.getApplianceCount() * corridorCount);
            }
        }
        return result;
    }

    public Float computeCurrentPowerConsumption() {
        return Stream.concat(mainCorridors.stream(), subCorridors.stream()).map(Corridor::computeCurrentPowerConsumption).reduce(0F, Float::sum);
    }

    public Boolean isPowerConsumptionAboveThreshold() {
        return computeCurrentPowerConsumption() > getMaxAllowedPowerConsumption();
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(System.lineSeparator()).append("Floor ").append(floorId).append(System.lineSeparator());
        builder.append("Consumption : ").append(computeCurrentPowerConsumption()).append(" | Limit : ").append(maxAllowedPowerConsumption).append(System.lineSeparator());
        mainCorridors.forEach(mc -> builder.append(mc.toString()));
        subCorridors.forEach(mc -> builder.append(mc.toString()));
        return builder.toString();

    }
}
