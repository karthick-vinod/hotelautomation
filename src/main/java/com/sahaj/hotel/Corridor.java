package com.sahaj.hotel;

import com.sahaj.config.Configuration;
import com.sahaj.config.ConfigEntity;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

@Data
public class Corridor {
    public enum CorridorType {MAIN, SUB}

    private Integer id;
    private CorridorType type;
    private List<Appliance> appliances = new ArrayList<>();
    private Boolean motionInProgress = false;

    public Corridor(Integer corridorId, CorridorType type) {
        this.id = corridorId;
        this.type = type;
        Map<Appliance.ApplianceType, ConfigEntity> config = Configuration.get(type);
        config.forEach((key, value) -> appliances.addAll(getAppliances(value)));
    }

    private List<Appliance> getAppliances(ConfigEntity configEntity) {
        List<Appliance> newAppliances = new ArrayList<>();
        IntStream.range(1, configEntity.getApplianceCount() + 1).forEachOrdered(
                i -> newAppliances.add(
                        new Appliance(i, configEntity.getApplianceType(), configEntity.getState(), configEntity.getAppliancePower())
                )
        );
        return newAppliances;
    }

    Float computeCurrentPowerConsumption() {
        return appliances.stream().map(Appliance::getCurrentPowerConsumption).reduce(0F, Float::sum);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(type.name()).append(" corridor ").append(id).append(" ");
        appliances.forEach(appliance -> builder.append(appliance.toString()));
        builder.append(" Consumption : ").append(computeCurrentPowerConsumption()).append(" Motion : ").append(getMotionInProgress());
        return builder.append(System.lineSeparator()).toString();
    }
}
