package com.sahaj.hotel;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class Appliance {
    public enum State {ON, OFF}

    public enum ApplianceType {AC, LIGHT}


    private Integer id;
    private ApplianceType type;
    private State state;
    private Float PowerConsumption;

    public Float getCurrentPowerConsumption() {
        return this.state == State.ON ? PowerConsumption : 0F;
    }

    public void turnOn() {
        this.setState(State.ON);
        System.out.println("Turning on");
        System.out.println(this);
    }

    public void turnOff() {
        this.setState(State.OFF);
        System.out.println("Turning off");
        System.out.println(this);
    }

    @Override
    public String toString() {
        return type.name() + " " + id + " : " + state.name() + " ";
    }
}
