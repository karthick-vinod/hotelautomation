package com.sahaj.controllers;

import com.sahaj.BaseTest;
import com.sahaj.config.ConfigEntity;
import com.sahaj.config.Configuration;
import com.sahaj.events.MotionSensorEvent;
import com.sahaj.hotel.Appliance;
import com.sahaj.hotel.Corridor;
import com.sahaj.hotel.Floor;
import com.sahaj.hotel.Hotel;
import org.junit.Test;

import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import static com.sahaj.events.enums.MotionSensorEventType.MOTION_DETECTED;
import static com.sahaj.events.enums.MotionSensorEventType.MOTION_HALTED;
import static com.sahaj.hotel.Appliance.ApplianceType.LIGHT;
import static com.sahaj.hotel.Appliance.State.OFF;
import static com.sahaj.hotel.Appliance.State.ON;
import static com.sahaj.hotel.Corridor.CorridorType.MAIN;
import static com.sahaj.hotel.Corridor.CorridorType.SUB;
import static org.junit.Assert.assertEquals;

public class HotelAutomationBaseControllerTest extends BaseTest {

    @Test
    public void shouldInitializeHotelWithCorrectCountOfEntities() {
        int floorCount = 3;
        int mainCorridorCount = 2;
        int subCorridorCount = 4;

        Hotel hotel = constructHotel(floorCount, mainCorridorCount, subCorridorCount);

        assertEquals(hotel.getFloors().size(), floorCount);
        hotel.getFloors().forEach(floor -> {
            assertEquals(floor.getMainCorridors().size(), mainCorridorCount);
            assertEquals(floor.getSubCorridors().size(), subCorridorCount);


            BiFunction<Corridor, Corridor.CorridorType, Boolean> assertCorridorEntityCounts = (Corridor corridor, Corridor.CorridorType corridorType) -> {
                assertEquals(corridor.getMotionInProgress(), false);

                for (Appliance.ApplianceType applianceType : Appliance.ApplianceType.values()) {
                    List<Appliance> appliancesOfAType = corridor.getAppliances().stream().filter(a -> a.getType() == applianceType).collect(Collectors.toList());
                    ConfigEntity applianceConfig = Configuration.get(corridorType, applianceType);

                    assertEquals((Integer) appliancesOfAType.size(), applianceConfig.getApplianceCount());

                    appliancesOfAType.forEach(appliance -> {
                        assertEquals(applianceConfig.getAppliancePower(), appliance.getPowerConsumption());
                        assertEquals(applianceConfig.getState(), appliance.getState());
                    });
                }
                return true;
            };

            floor.getMainCorridors().forEach(corridor -> assertCorridorEntityCounts.apply(corridor, MAIN));
            floor.getSubCorridors().forEach(corridor -> assertCorridorEntityCounts.apply(corridor, SUB));
        });
    }

    @Test
    public void shouldProcessMotionDetectedEvent() {
        Hotel hotel = constructHotel(3, 1, 4);
        HotelAutomationBaseController hotelController = constructController(hotel);

        MotionSensorEvent event = constructMotionSensorEvent(hotel, MOTION_DETECTED, 1, 3);
        Floor floor = getFloor(hotel, 1);
        Corridor subCorridor = getSubCorridor(hotel, 1, 3);

        assertEquals(subCorridor.getMotionInProgress(), false);
        Float initialPowerConsumption = floor.computeCurrentPowerConsumption();

        hotelController.processEvent(event);

        assertEquals(subCorridor.getMotionInProgress(), true);
        assertEquals(floor.computeCurrentPowerConsumption().intValue(), (int) (initialPowerConsumption - 5));
        subCorridor.getAppliances().stream().filter(a -> a.getType() == LIGHT).forEach(a -> assertEquals(a.getState(), ON));
        assertEquals(floor.getMaxAllowedPowerConsumption() >= floor.computeCurrentPowerConsumption(), true);
    }

    @Test
    public void shouldProcessMotionHaltedEvent() {
        Hotel hotel = constructHotel(3, 1, 4);
        HotelAutomationBaseController hotelController = constructController(hotel);

        MotionSensorEvent event = constructMotionSensorEvent(hotel, MOTION_DETECTED, 1, 3);
        hotelController.processEvent(event);

        assertEquals(true, getSubCorridor(hotel, 1, 3).getMotionInProgress());

        event = constructMotionSensorEvent(hotel, MOTION_HALTED, 1, 3);
        Floor floor = getFloor(hotel, 1);
        Corridor subCorridor = getSubCorridor(hotel, 1, 3);

        hotelController.processEvent(event);

        assertEquals(false, getSubCorridor(hotel, 1, 3).getMotionInProgress());
        assertEquals(subCorridor.getMotionInProgress(), false);

        subCorridor.getAppliances().stream().filter(a -> a.getType() == LIGHT).forEach(a -> assertEquals(a.getState(), OFF));
        assertEquals(floor.getMaxAllowedPowerConsumption() >= floor.computeCurrentPowerConsumption(), true);
    }
}
