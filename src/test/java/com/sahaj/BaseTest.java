package com.sahaj;

import com.sahaj.controllers.HotelAutomationBaseController;
import com.sahaj.events.MotionSensorEvent;
import com.sahaj.events.enums.MotionSensorEventType;
import com.sahaj.hotel.Corridor;
import com.sahaj.hotel.Floor;
import com.sahaj.hotel.Hotel;

public class BaseTest {

    public static Hotel constructHotel(Integer floors, Integer mainCorridors, Integer subCorridors) {
        return new Hotel(floors, mainCorridors, subCorridors);
    }

    public static HotelAutomationBaseController constructController(Hotel hotel) {
        return new HotelAutomationBaseController(hotel);
    }

    public static Floor getFloor(Hotel hotel, Integer index) {
        return hotel.getFloors().get(index);
    }

    public static Corridor getSubCorridor(Hotel hotel, Integer floorIndex, Integer subCorridorIndex) {
        return getFloor(hotel, floorIndex).getSubCorridors().get(subCorridorIndex);
    }

    public static MotionSensorEvent constructMotionSensorEvent(Hotel hotel, MotionSensorEventType eventType, Integer floorIndex, Integer subCorridorIndex) {
        return new MotionSensorEvent(eventType, getFloor(hotel, floorIndex), getSubCorridor(hotel, floorIndex, subCorridorIndex));
    }
}
