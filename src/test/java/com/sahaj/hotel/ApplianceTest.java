package com.sahaj.hotel;

import org.junit.Test;

import static com.sahaj.hotel.Appliance.ApplianceType.AC;
import static com.sahaj.hotel.Appliance.State.OFF;
import static com.sahaj.hotel.Appliance.State.ON;
import static org.junit.Assert.assertEquals;

public class ApplianceTest {

    @Test
    public void getCurrentPowerConsumptionTest() {
        Appliance onAppliance = new Appliance(1, AC, ON, 5.3F);
        Appliance offAppliance = new Appliance(1, AC, OFF, 5.3F);

        assertEquals((Float) 5.3F, onAppliance.getCurrentPowerConsumption());
        assertEquals((Float) 0.0F, offAppliance.getCurrentPowerConsumption());

//        assertEquals((Float) 5.3F, new Appliance(1, AC, ON, 5.3F).getPowerConsumption());
//        new Appliance(1, AC, OFF, 5.3F).getPowerConsumption();
//        assertEquals((Float) 0F, new Appliance(1, AC, OFF, 5.3F).getPowerConsumption());
    }

    @Test
    public void turnOnTest() {
        Appliance appliance = new Appliance(1, AC, OFF, 5.3F);
        appliance.turnOn();
        assertEquals(ON, appliance.getState());
    }

    @Test
    public void turnOffTest() {
        Appliance appliance = new Appliance(1, AC, ON, 5.3F);
        appliance.turnOff();
        assertEquals(OFF, appliance.getState());
    }
}
