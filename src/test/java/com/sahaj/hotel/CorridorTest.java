package com.sahaj.hotel;

import com.sahaj.config.Configuration;
import org.junit.Assert;
import org.junit.Test;

import static com.sahaj.hotel.Corridor.CorridorType.MAIN;
import static org.junit.Assert.assertEquals;

public class CorridorTest {

    @Test
    public void newCorridorMustInitializeAppliances() {
        Corridor corridor = new Corridor(1, MAIN);
        assertEquals(corridor.getMotionInProgress(), false);
        assertEquals(Configuration.get(MAIN).values().stream().mapToInt(e -> e.getApplianceCount()).sum(), corridor.getAppliances().size());
    }

    @Test
    public void computeCurrentPowerConsumptionTest() {
        Corridor corridor = new Corridor(1, MAIN);
        assertEquals(15, corridor.computeCurrentPowerConsumption().intValue());
    }
}
