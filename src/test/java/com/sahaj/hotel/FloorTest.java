package com.sahaj.hotel;

import org.junit.Test;

import static com.sahaj.hotel.Appliance.ApplianceType.LIGHT;
import static org.junit.Assert.assertEquals;

public class FloorTest {
    @Test
    public void computeMaxAllowedPowerConsumptionTest() {
        Floor floor = new Floor(1, 2, 3);
        assertEquals(2, floor.getMainCorridors().size());
        assertEquals(3, floor.getSubCorridors().size());

        assertEquals(60, floor.getMaxAllowedPowerConsumption().intValue());
    }

    @Test
    public void computeCurrentPowerConsumptionTest() {
        Floor floor = new Floor(1, 2, 3);
        floor.getSubCorridors().get(0).getAppliances().stream().filter(a -> a.getType() == LIGHT).findAny().ifPresent(a -> a.turnOn());
        assertEquals(65, floor.computeCurrentPowerConsumption().intValue());
    }

    @Test
    public void isPowerConsumptionAboveThresholdTest() {
        Floor floor = new Floor(1, 2, 3);

        assertEquals(false, floor.isPowerConsumptionAboveThreshold());
        floor.getSubCorridors().get(0).getAppliances().stream().filter(a -> a.getType() == LIGHT).findAny().ifPresent(a -> a.turnOn());
        assertEquals(true, floor.isPowerConsumptionAboveThreshold());
    }
}
